use anyhow::{Context, Result};
use std::{
    ffi::{c_void, CString},
    path::{Path, PathBuf},
};

use windows::Win32::{
    Foundation::{HANDLE, RECT},
    UI::{
        Shell::{FOLDERID_Profile, SHGetKnownFolderPath, KF_FLAG_DEFAULT},
        WindowsAndMessaging::{
            GetDesktopWindow, GetWindowRect, SystemParametersInfoA, SPIF_SENDCHANGE,
            SPIF_UPDATEINIFILE, SPI_SETDESKWALLPAPER,
        },
    },
};

pub fn get_desktop_size() -> Option<(u32, u32)> {
    unsafe {
        let mut rect = <RECT as Default>::default();
        let hwnd = GetDesktopWindow();
        if let Ok(()) = GetWindowRect(hwnd, &mut rect) {
            Some((
                (rect.left + rect.right) as u32,
                (rect.top + rect.bottom) as u32,
            ))
        } else {
            None
        }
    }
}

pub fn get_destination_file() -> Result<PathBuf> {
    unsafe {
        let result = SHGetKnownFolderPath(&FOLDERID_Profile, KF_FLAG_DEFAULT, HANDLE::default())?
            .to_string()?;

        Ok(PathBuf::from(result).join("wallpaper.bmp"))
    }
}

pub fn set_wallpaper(path: &Path) -> Result<()> {
    let c_path = CString::new(path.to_str().context("")?)?;

    unsafe {
        SystemParametersInfoA(
            SPI_SETDESKWALLPAPER,
            0,
            Some(c_path.as_ptr() as *mut c_void),
            SPIF_UPDATEINIFILE | SPIF_SENDCHANGE,
        )
        .context("Unable to set wallpaper")
    }
}
