mod cli;
mod win;

use std::{fs::File, io::BufWriter, process::ExitCode};

use anyhow::Result;
use clap::Parser;
use cli::CliArgs;
use lib::{image::bmp_image::BmpImage, run};
use win::{get_destination_file, set_wallpaper};

fn main() -> ExitCode {
    match CliArgs::parse()
        .to_options()
        .and_then(|opts| save_image(run(opts)))
    {
        Ok(()) => ExitCode::SUCCESS,
        Err(e) => {
            eprintln!("{}", e);
            ExitCode::FAILURE
        }
    }
}

fn save_image(image: BmpImage) -> Result<()> {
    let path = get_destination_file()?;

    let file = File::create(&path)?;
    image.to_writer(&mut BufWriter::new(file))?;

    set_wallpaper(&path)?;

    Ok(())
}
