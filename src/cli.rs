use anyhow::{Context, Result};
use clap::Parser;
use lib::{colors::gradients::GRADIENTS, options::Options};
use rand::{rngs::StdRng, Rng, SeedableRng};

use crate::win::get_desktop_size;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct CliArgs {
    /// Image width [default: desktop width]
    #[arg(long, value_parser = clap::value_parser!(u32).range(1..))]
    width: Option<u32>,

    /// Image height [default: desktop height]
    #[arg(long, value_parser = clap::value_parser!(u32).range(1..))]
    height: Option<u32>,

    /// Triangle variation [default: rand(128, 256)]
    #[arg(long)]
    scale: Option<f64>,

    /// Random seed
    #[arg(long)]
    rng_seed: Option<u64>,

    /// Gradient to use
    #[arg(long, value_parser = clap::value_parser!(u32).range(0..(GRADIENTS.len() - 1) as i64))]
    gradient: Option<u32>,
}

impl CliArgs {
    pub fn to_options(&self) -> Result<Options> {
        let mut rng = self
            .rng_seed
            .map(StdRng::seed_from_u64)
            .unwrap_or_else(StdRng::from_entropy);

        let desktop_size = get_desktop_size();
        let width = self
            .width
            .or_else(|| desktop_size.map(|(w, _)| w))
            .context("Unable to determine desktop size")?;
        let height = self
            .height
            .or_else(|| desktop_size.map(|(_, h)| h))
            .context("Unable to determine desktop size")?;

        Ok(Options {
            width,
            height,
            scale: self.scale.unwrap_or_else(|| rng.gen_range(128.0..256.0)),
            colors: self
                .gradient
                .map(|i| &GRADIENTS[i as usize])
                .unwrap_or_else(|| &GRADIENTS[rng.gen_range(0..GRADIENTS.len())]),
            rng,
        })
    }
}
