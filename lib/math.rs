use num_traits::{Float, Num};

use crate::geometry::point::Point;

/// Check that two floats are approximately equal.
pub fn float_approx_eq<T: Float>(a: T, b: T) -> bool {
    a == b || (a - b).abs() < T::epsilon()
}

/// Round a number up to the next multiple of another number.
pub fn round_up_to_multiple_of<T>(n: T, multiple: T) -> T
where
    T: Num + Copy + PartialOrd,
{
    ((n + multiple - T::one()) / multiple) * multiple
}

/// Round a number down to the previous multiple of another number.
pub fn round_down_to_multiple_of<T>(n: T, multiple: T) -> T
where
    T: Num + Copy + PartialOrd,
{
    n - n % multiple
}

/// Check whether point C is left, right, or on the line A-B.
pub fn orient2d(a: &Point<f32>, b: &Point<f32>, c: &Point<f32>) -> f32 {
    (c.x - a.x) * (b.y - a.y) - (c.y - a.y) * (b.x - a.x)
}
