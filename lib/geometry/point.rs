use num_traits::Float;

use crate::math::float_approx_eq;

/// 2D Point on a plane.
#[derive(Debug, Default, Clone, Copy)]
pub struct Point<T> {
    /// X coordinate of the point.
    pub x: T,
    /// Y coordinate of the point.
    pub y: T,
}

impl<T> Point<T> {
    /// Create a new Point with coordinates (x,y).
    pub const fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T> Point<T>
where
    T: Float,
{
    /// Calculate this Point's distance form another Point
    pub fn dist(&self, other: &Self) -> T {
        ((self.x - other.x).powi(2) + (self.y - other.y).powi(2)).sqrt()
    }

    /// Calculate this Point's distance from the origin.
    pub fn dist_origin(&self) -> T {
        ((self.x * self.x) + (self.y * self.y)).sqrt()
    }
}

impl<T> std::ops::Add for Point<T>
where
    T: std::ops::Add<Output = T>,
{
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T> std::ops::Sub for Point<T>
where
    T: std::ops::Sub<Output = T>,
{
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl<T> PartialEq for Point<T>
where
    T: Float,
{
    fn eq(&self, other: &Self) -> bool {
        float_approx_eq(self.x, other.x) && float_approx_eq(self.y, other.y)
    }
}

impl<T> Eq for Point<T> where T: Float {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dist_should_equal_0_for_equal_points() {
        let a = Point::new(0., 0.);
        let b = Point::new(10., 0.);
        let c = Point::new(0., 10.);
        let d = Point::new(10., 10.);

        assert!(float_approx_eq(a.dist(&a), 0.));
        assert!(float_approx_eq(b.dist(&b), 0.));
        assert!(float_approx_eq(c.dist(&c), 0.));
        assert!(float_approx_eq(d.dist(&d), 0.));
    }

    #[test]
    fn dist_should_be_correct() {
        let a = Point::new(-1., -1.);
        let b = Point::new(-1., -1.);
        let c = Point::new(3., -1.);
        let d = Point::new(-1., 3.);
        let e = Point::new(14., 19.);

        assert!(float_approx_eq(a.dist(&b), 0.));
        assert!(float_approx_eq(a.dist(&c), 4.));
        assert!(float_approx_eq(a.dist(&d), 4.));
        assert!(float_approx_eq(a.dist(&e), 25.));
    }

    #[test]
    fn eq_should_approximate_floats() {
        let a = Point::new(0., 1.);
        let b = Point::new(0., 1.);
        let c = Point::new(1. + 2., 1. + 2.);
        let d = Point::new(3., 3.);

        assert_eq!(a, b);
        assert_eq!(c, d);
    }
}
