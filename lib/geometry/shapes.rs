use num_traits::Float;

use super::point::Point;

/// Edge from a to b.
#[derive(Debug, Clone)]
pub struct Edge<T>
where
    T: Float,
{
    /// Start point.
    pub a: Point<T>,
    /// End Point.
    pub b: Point<T>,
}

impl<T> Edge<T>
where
    T: Float,
{
    /// Create a new edge form a to b.
    pub const fn new(a: Point<T>, b: Point<T>) -> Self {
        Self { a, b }
    }
}

impl<T> PartialEq for Edge<T>
where
    T: Float,
{
    fn eq(&self, other: &Self) -> bool {
        (self.a == other.a && self.b == other.b) || (self.a == other.b && self.b == other.a)
    }
}
impl<T> Eq for Edge<T> where T: Float {}

/// Circle with given center and radius.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Circle<T>
where
    T: Float,
{
    /// Center fo the circle.
    pub center: Point<T>,
    /// Radius of the circle.
    pub radius: T,
}

/// Representation of a rectangle.
pub struct Rect<T> {
    /// Top left vertex of the rectangle.
    pub top_left: Point<T>,
    /// Bottom right vertex of the rectangle.
    pub bottom_right: Point<T>,
}

impl Rect<i32> {
    /// Create a new rectangle with the given opposite vertices.
    pub const fn new(top_left: Point<i32>, bottom_right: Point<i32>) -> Self {
        Self {
            top_left,
            bottom_right,
        }
    }

    /// Get the rectangle's width.
    pub const fn width(&self) -> i32 {
        (self.bottom_right.x - self.top_left.x).abs()
    }

    /// Get the rectangle's height.
    pub const fn height(&self) -> i32 {
        (self.bottom_right.y - self.top_left.y).abs()
    }

    /// Return a new rectangle clipped against the given box.
    pub fn clipped(&self, bounding_box: &Self) -> Self {
        Self {
            top_left: Point::new(
                i32::max(self.top_left.x, bounding_box.top_left.x),
                i32::max(self.top_left.y, bounding_box.top_left.y),
            ),
            bottom_right: Point::new(
                i32::min(self.bottom_right.x, bounding_box.bottom_right.x),
                i32::min(self.bottom_right.y, bounding_box.bottom_right.y),
            ),
        }
    }
}

/// Representation of a triangle.
#[derive(Debug, Clone, PartialEq)]
pub struct Triangle {
    /// First vertex.
    p0: Point<f64>,
    /// Second vertex.
    p1: Point<f64>,
    /// Third vertex.
    p2: Point<f64>,
    /// Circumcicle of the triangle.
    /// Used to cache its calculation.
    circle: Circle<f64>,
}

impl Triangle {
    /// Create a new triangle with the given vertices.
    pub fn new(p0: Point<f64>, p1: Point<f64>, p2: Point<f64>) -> Self {
        let ax = p1.x - p0.x;
        let ay = p1.y - p0.y;
        let bx = p2.x - p0.x;
        let by = p2.y - p0.y;

        let m = p1.y.mul_add(p1.y, p1.x * p1.x - p0.x * p0.x) - p0.y * p0.y;
        let u = p2.y.mul_add(p2.y, p2.x * p2.x - p0.x * p0.x) - p0.y * p0.y;
        let s = 1. / (2. * (ax * by - ay * bx));

        let cx = (p2.y - p0.y).mul_add(m, (p0.y - p1.y) * u) * s;
        let cy = (p0.x - p2.x).mul_add(m, (p1.x - p0.x) * u) * s;

        let dp = Point::new(p0.x - cx, p0.y - cy);

        Self {
            p0,
            p1,
            p2,
            circle: Circle {
                center: Point::new(cx, cy),
                radius: dp.dist_origin(),
            },
        }
    }

    /// Get the first vertex.
    pub const fn p0(&self) -> Point<f64> {
        self.p0
    }

    /// Get the second vertex.
    pub const fn p1(&self) -> Point<f64> {
        self.p1
    }

    /// Get the third vertex.
    pub const fn p2(&self) -> Point<f64> {
        self.p2
    }

    /// Get the circumcircle of the triangle.
    pub const fn circumcircle(&self) -> &Circle<f64> {
        &self.circle
    }

    /// Get the triangle's centroid.
    fn centroid(&self) -> Point<f64> {
        Point::new(
            (self.p0.x + self.p1.x + self.p2.x) / 3.,
            (self.p0.y + self.p1.y + self.p2.y) / 3.,
        )
    }

    /// Calculate the distance fo the triangle's centroid form another point.
    pub fn distance_from(&self, point: &Point<i32>) -> f64 {
        let centroid = self.centroid();

        (centroid.x - f64::from(point.x)).hypot(centroid.y - f64::from(point.y))
    }

    /// Calculate the triangle's bounding box.
    pub fn bounding_box(&self) -> Rect<i32> {
        // Triangle bounding box
        let min_x = f64::min(f64::min(self.p0.x, self.p1.x), self.p2.x).round() as i32;
        let min_y = f64::min(f64::min(self.p0.y, self.p1.y), self.p2.y).round() as i32;
        let max_x = f64::max(f64::max(self.p0.x, self.p1.x), self.p2.x).round() as i32;
        let max_y = f64::max(f64::max(self.p0.y, self.p1.y), self.p2.y).round() as i32;

        Rect::new(Point::new(min_x, min_y), Point::new(max_x + 1, max_y + 1))
    }

    /// Return an array of the triangle's points as f32 values.
    pub const fn points_as_f32(&self) -> [Point<f32>; 3] {
        [
            Point::new(self.p0.x as f32, self.p0.y as f32),
            Point::new(self.p1.x as f32, self.p1.y as f32),
            Point::new(self.p2.x as f32, self.p2.y as f32),
        ]
    }
}
