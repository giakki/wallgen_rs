use super::{
    point::Point,
    shapes::{Edge, Triangle},
};
use rand::Rng;

/// Generate triangles of given scale filling a rectangle of given width and height.
pub fn generate_triangles<R: Rng>(
    rng: &mut R,
    width: u32,
    height: u32,
    scale: f64,
) -> Vec<Triangle> {
    // Ref. https://en.wikipedia.org/wiki/Bowyer%E2%80%93Watson_algorithm
    // The pseudo code is present above each corresponding line.

    let x_min = -scale as i32;
    let x_max = width as i32 + scale as i32;
    let y_min = -scale as i32;
    let y_max = height as i32 + scale as i32;

    // triangulation := empty triangle mesh data structure
    // add super-triangle to triangulation
    let super_triangle = make_super_triangle(x_min, x_max, y_min, y_max);
    let mut triangles = vec![super_triangle.clone()];

    // for each point in pointList do
    for y in (y_min..y_max).step_by(scale as usize) {
        for x in (x_min..x_max).step_by(scale as usize) {
            let pt = Point::new(
                f64::from(x) + rng.gen_range(0.0..(scale / 2.)),
                f64::from(y) + rng.gen_range(0.0..(scale / 2.)),
            );
            // badTriangles := empty set
            let mut new_edges = Vec::new();

            // first find all the triangles that are no longer valid due to the insertion
            // for each triangle in triangulation do
            triangles.retain(|tri| {
                let circle = &tri.circumcircle();
                // if point is inside circumcircle of triangle
                let dist = pt.dist(&circle.center);
                if (dist - circle.radius) <= f64::EPSILON {
                    // add triangle to badTriangles
                    new_edges.push(Edge::new(tri.p0(), tri.p1()));
                    new_edges.push(Edge::new(tri.p1(), tri.p2()));
                    new_edges.push(Edge::new(tri.p0(), tri.p2()));
                    return false;
                }
                true
            });

            // find the boundary of the polygonal hole
            // polygon := empty set
            let mut duplicated_edges = vec![false; new_edges.len()];
            // for each triangle in badTriangles do
            for (i1, edge1) in new_edges.iter().enumerate() {
                for (i2, edge2) in new_edges.iter().enumerate() {
                    if i1 == i2 {
                        continue;
                    }

                    // if edge is not shared by any other triangles in badTriangles
                    if edge1 == edge2 {
                        // add edge to polygon
                        duplicated_edges[i1] = true;
                        duplicated_edges[i2] = true;
                    }
                }
            }

            // remove them from the data structure
            // for each triangle in badTriangles do
            //   remove triangle from triangulation
            let mut i = 0;
            new_edges.retain(|_| {
                i += 1;
                !duplicated_edges[i - 1]
            });

            // re-triangulate the polygonal hole
            // for each edge in polygon do
            for e in new_edges {
                // add newTri to triangulation
                // newTri := form a triangle from edge to point
                triangles.push(Triangle::new(e.a, e.b, Point::new(pt.x, pt.y)));
            }
        }
    }

    // done inserting points, now clean up
    // for each triangle in triangulation
    //   if triangle contains a vertex from original super-triangle
    //     remove triangle from triangulation
    triangles.retain(|tri| {
        let shares_vertex = (tri.p0() == super_triangle.p0()
            || tri.p1() == super_triangle.p0()
            || tri.p2() == super_triangle.p0())
            || (tri.p0() == super_triangle.p1()
                || tri.p1() == super_triangle.p1()
                || tri.p2() == super_triangle.p1())
            || (tri.p0() == super_triangle.p2()
                || tri.p1() == super_triangle.p2()
                || tri.p2() == super_triangle.p2());

        !shares_vertex
    });

    triangles
}

/// Get a triangle containing all the points of the triangulation.
fn make_super_triangle(x_min: i32, x_max: i32, y_min: i32, y_max: i32) -> Triangle {
    let dx = f64::from(x_max - x_min);
    let dy = f64::from(y_max - y_min);
    let d_max = f64::max(dx, dy);
    let mid_x = f64::from(x_min + x_max) / 2.;
    let mid_y = f64::from(y_min + y_max) / 2.;

    Triangle::new(
        Point::new(mid_x - 20. * d_max, mid_y - d_max),
        Point::new(mid_x, d_max.mul_add(20., mid_y)),
        Point::new(d_max.mul_add(20., mid_x), mid_y - d_max),
    )
}

#[cfg(test)]
mod tests {
    use rand::{rngs::StdRng, SeedableRng};

    use super::*;

    #[test]
    fn test_triangulation_simple() {
        let tris = generate_triangles(&mut StdRng::seed_from_u64(0), 100, 100, 100.);

        assert_eq!(tris.len(), 9);
    }
}
