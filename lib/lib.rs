pub mod colors {
    pub mod gradients;
    pub mod oklab;
    pub mod oklab_simd;
    pub mod pattern;
    pub mod rgba;
}
pub mod geometry {
    pub mod point;
    pub mod shapes;
    pub mod triangulation;
}
pub mod image {
    pub mod aligned_buffer;
    pub mod bmp_image;
}
pub mod rasterizer {
    pub mod barycentric;
    pub mod barycentric_iterator;
}

pub mod math;
pub mod options;

use colors::pattern::{Pattern, RadialGradient};
use geometry::{point::Point, triangulation::generate_triangles};
use image::bmp_image::BmpImage;
use options::Options;
use rasterizer::barycentric::rasterize_triangle;

/// Main library function.
///
/// Generates the wallpaper with the given options.
pub fn run(mut options: Options) -> BmpImage {
    let triangles = generate_triangles(
        &mut options.rng,
        options.width,
        options.height,
        options.scale,
    );
    let mut image = BmpImage::new(options.width, options.height);

    let pattern = RadialGradient::new(
        Point::<i32>::new(0, 0),
        Point::<i32>::new(options.width as i32, options.height as i32),
        options.colors,
    );

    for triangle in triangles {
        rasterize_triangle(&mut image, &triangle, pattern.apply(&triangle));
    }

    image
}
