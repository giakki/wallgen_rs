use crate::{
    colors::rgba::Rgba,
    geometry::{point::Point, shapes::Triangle},
    image::bmp_image::BmpImage,
    math::{round_down_to_multiple_of, round_up_to_multiple_of},
};
use std::arch::x86_64::*;

use super::barycentric_iterator::BarycentricIterator;

/// Rasterize a triangle onto a given image.
/// Perf: seems like most of the time is spent setting up and cleaning up the stack.
pub fn rasterize_triangle(canvas: &mut BmpImage, triangle: &Triangle, color: Rgba) {
    // Get the drawable area by clipping the rectangle's bounding box against the image's bounding box.
    let mut area = triangle.bounding_box().clipped(&canvas.rect());
    // Align x to 32 bytes, for faster memory accesses.
    // Since a canvas row's length is a multiple of 8, we never end up out of bounds.
    area.top_left.x = round_down_to_multiple_of(area.top_left.x, 8);
    area.bottom_right.x = round_up_to_multiple_of(area.bottom_right.x, 8);

    // Get the barycentric coordinates at the origin
    let origin = Point::new(area.top_left.x as f32, area.top_left.y as f32);
    let [v0, v1, v2] = triangle.points_as_f32();
    let mut w_row = BarycentricIterator::new(&v0, &v1, &v2, &origin);

    for y in area.top_left.y..area.bottom_right.y {
        let mut w = w_row.clone();

        for x in (area.top_left.x..=(area.bottom_right.x - 8)).step_by(8) {
            // Barycentric coordinates of these points.
            let [w0, w1, w2] = w.get();

            // Safety: safe.
            unsafe {
                // The points are inside the triangle if their barycentric coordinates are > 0, if the triangle's
                //   vertices are in ccw order, and < 0 if not
                // Little trick: instead of three compares, do an `or`, since we only care about the sign bit.
                // Not much faster, but it is considerably cleaner.
                let write_mask = _mm256_cmp_ps(
                    _mm256_or_ps(w0, _mm256_or_ps(w1, w2)),
                    _mm256_setzero_ps(),
                    _CMP_NLT_UQ,
                );

                // Write 8 pixels at a time. The mask ensures we write only the pixels inside the triangle.
                // Blending between old values and new values appears to be faster than a masked store.
                let ptr = canvas.pixels_mut(x as u32, y as u32).cast::<f32>();
                let old_value = _mm256_load_ps(ptr);
                let new_value = _mm256_blendv_ps(
                    old_value,
                    _mm256_set1_ps(f32::from_bits(color.rgba)),
                    write_mask,
                );

                _mm256_store_ps(ptr, new_value);
            }
            w.step_x();
        }
        w_row.step_y();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::image::bmp_image::PIXEL_DATA_OFFSET;
    use std::io::BufWriter;

    fn rasterize_to_string(bmp: &BmpImage) -> Vec<String> {
        let mut buf = BufWriter::new(Vec::new());
        bmp.to_writer(&mut buf).unwrap();

        let buf = buf.into_inner().unwrap();
        let buf = buf
            .iter()
            .skip(PIXEL_DATA_OFFSET as usize)
            .collect::<Vec<_>>();

        buf.chunks_exact(bmp.width() as usize * 4)
            .rev()
            .map(|row| {
                row.chunks_exact(4)
                    .map(|color| if *color[0] != 0 { '#' } else { ' ' })
                    .collect()
            })
            .collect()
    }

    #[test]
    fn rasterize_ex_1() {
        let mut image = BmpImage::new(8, 8);
        let triangle = Triangle::new(Point::new(0., 0.), Point::new(0., 7.), Point::new(7., 7.));
        let color = Rgba::new(0x12, 0x34, 0x56);

        rasterize_triangle(&mut image, &triangle, color);

        #[rustfmt::skip]
        assert_eq!(
            rasterize_to_string(&image),
            vec![
                "#       ",
                "##      ",
                "###     ",
                "####    ",
                "#####   ",
                "######  ",
                "####### ",
                "########",
            ]
        );
    }

    #[test]
    fn rasterize_ex_2() {
        let mut image = BmpImage::new(9, 9);
        let triangle = Triangle::new(Point::new(1., 1.), Point::new(1., 8.), Point::new(8., 8.));
        let color = Rgba::new(0x12, 0x34, 0x56);

        rasterize_triangle(&mut image, &triangle, color);

        #[rustfmt::skip]
        assert_eq!(
            rasterize_to_string(&image),
            vec![
                "         ",
                " #       ",
                " ##      ",
                " ###     ",
                " ####    ",
                " #####   ",
                " ######  ",
                " ####### ",
                " ########",
            ]
        );
    }

    #[test]
    fn rasterize_ex_3() {
        let mut image = BmpImage::new(11, 11);
        let triangle = Triangle::new(
            Point::new(5., 0.),
            Point::new(0., 10.),
            Point::new(10., 10.),
        );
        let color = Rgba::new(0x12, 0x34, 0x56);

        rasterize_triangle(&mut image, &triangle, color);

        #[rustfmt::skip]
        assert_eq!(
            rasterize_to_string(&image),
            vec![
                "     #     ",
                "     #     ",
                "    ###    ",
                "    ###    ",
                "   #####   ",
                "   #####   ",
                "  #######  ",
                "  #######  ",
                " ######### ",
                " ######### ",
                "###########",
            ]
        );
    }

    #[test]
    fn rasterize_ex_4() {
        let mut image = BmpImage::new(8, 8);
        let triangle = Triangle::new(Point::new(0., 7.), Point::new(0., 0.), Point::new(7., 7.));
        let color = Rgba::new(0x12, 0x34, 0x56);

        rasterize_triangle(&mut image, &triangle, color);

        #[rustfmt::skip]
        assert_eq!(
            rasterize_to_string(&image),
            vec![
                "#       ",
                "##      ",
                "###     ",
                "####    ",
                "#####   ",
                "######  ",
                "####### ",
                "########",
            ]
        );
    }
}
