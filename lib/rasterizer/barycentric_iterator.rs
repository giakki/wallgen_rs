use crate::{geometry::point::Point, math::orient2d};
use std::arch::x86_64::*;

#[derive(Debug, Clone)]
struct BarycentricEdgeIterator {
    one_step_x: __m256,
    one_step_y: __m256,
    curr: __m256,
}

impl BarycentricEdgeIterator {
    pub fn new(b: Point<f32>, c: Point<f32>, a: Point<f32>, sign: f32) -> Self {
        // If we take the orient2d formula:
        //   f = (c.x - a.x) * (b.y - a.y) - (c.y - a.y) * (b.x - a.x)
        // We can rewrite it as:
        //   f = a.x (b.y - c.y) + a.y (c.x - b.x) + b.x * c.y - b.y * c.x
        // Invert the sign:
        //   f = a.x (c.y - b.y) + a.y (b.x - c.x) + b.y * c.x - b.x * c.y
        // Group:
        //   x_factor = c.y - b.y
        //   y_factor = b.x - c.x
        //   constant_factor = b.y * c.x - b.x * c.y
        //   f = a.x * A + a.y * B + C
        // That is, every step of the loop (increasing a.x by 8 or a.y by 1) we just need to add
        //   8 * x_factor or 1 * y_factor

        let x_factor = (c.y - b.y) * sign;
        let y_factor = (b.x - c.x) * sign;
        let constant_factor = (b.y * c.x - b.x * c.y) * sign;

        // Safety: always safe.
        unsafe {
            // Step deltas
            let one_step_x = _mm256_set1_ps(x_factor * 8.);
            let one_step_y = _mm256_set1_ps(y_factor * 1.);

            // x/y values for initial pixel block
            let x = _mm256_add_ps(
                _mm256_set1_ps(a.x),
                _mm256_set_ps(7., 6., 5., 4., 3., 2., 1., 0.),
            );

            let y = _mm256_set1_ps(a.y);

            let curr = _mm256_add_ps(
                _mm256_mul_ps(_mm256_set1_ps(x_factor), x),
                _mm256_add_ps(
                    _mm256_mul_ps(_mm256_set1_ps(y_factor), y),
                    _mm256_set1_ps(constant_factor),
                ),
            );

            // Edge function values at origin
            Self {
                one_step_x,
                one_step_y,
                curr,
            }
        }
    }
}

impl BarycentricEdgeIterator {
    pub const fn curr(&self) -> __m256 {
        self.curr
    }

    pub fn step_x(&mut self) {
        self.curr = unsafe { _mm256_add_ps(self.curr, self.one_step_x) };
    }

    pub fn step_y(&mut self) {
        self.curr = unsafe { _mm256_add_ps(self.curr, self.one_step_y) };
    }
}

#[derive(Debug, Clone)]
pub struct BarycentricIterator {
    e12: BarycentricEdgeIterator,
    e20: BarycentricEdgeIterator,
    e01: BarycentricEdgeIterator,
}

impl BarycentricIterator {
    pub fn new(v0: &Point<f32>, v1: &Point<f32>, v2: &Point<f32>, origin: &Point<f32>) -> Self {
        let ccw_sign = orient2d(v0, v1, v2).signum();

        Self {
            e12: BarycentricEdgeIterator::new(*v1, *v2, *origin, ccw_sign),
            e20: BarycentricEdgeIterator::new(*v2, *v0, *origin, ccw_sign),
            e01: BarycentricEdgeIterator::new(*v0, *v1, *origin, ccw_sign),
        }
    }

    pub const fn get(&self) -> [__m256; 3] {
        [self.e12.curr(), self.e20.curr(), self.e01.curr()]
    }

    pub fn step_x(&mut self) {
        self.e01.step_x();
        self.e20.step_x();
        self.e12.step_x();
    }

    pub fn step_y(&mut self) {
        self.e01.step_y();
        self.e20.step_y();
        self.e12.step_y();
    }
}
