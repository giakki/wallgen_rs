use rand::rngs::StdRng;

use crate::colors::oklab::OkLab;

/// Options used by the main library.
#[derive(Debug, Clone)]
pub struct Options {
    /// Width of the generasted image.
    pub width: u32,
    /// Height of the generated image.
    pub height: u32,
    /// Size fo the triangles generated.
    pub scale: f64,
    /// Gradient used to fill the triangles.
    pub colors: &'static [OkLab],
    /// PRNG.
    pub rng: StdRng,
}
