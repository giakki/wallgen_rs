use super::aligned_buffer::AlignedBuffer;
use crate::{
    colors::rgba::Rgba,
    geometry::{point::Point, shapes::Rect},
    math::round_up_to_multiple_of,
};
use std::io::Write;

/// Size of the BMP header.
const BMP_HEADER_SIZE: u8 = 14;
/// Size fo the BMP v5 header.
const DIB_HEADER_SIZE: u8 = 124;
/// Offset in bytes of the pixel array from the image start.
pub const PIXEL_DATA_OFFSET: u8 = BMP_HEADER_SIZE + DIB_HEADER_SIZE;

/// Representation of a 32-bit BMP image.
pub struct BmpImage {
    /// Image width.
    width: u32,
    /// Image height.
    height: u32,
    /// Pixel data.
    image_data: AlignedBuffer<u32, 32>,
}

impl BmpImage {
    /// Create a new image width given width and height, filled with black.
    pub fn new(width: u32, height: u32) -> Self {
        // Round the width to the next multiple of 8, so that each row is 32-bit aligned.
        let row_width = round_up_to_multiple_of(width, 8);
        Self {
            width,
            height,
            image_data: AlignedBuffer::new((row_width * height) as usize),
        }
    }

    /// Get the image's width.
    pub const fn width(&self) -> u32 {
        self.width
    }

    /// Get the image's height.
    pub const fn height(&self) -> u32 {
        self.height
    }

    /// Get the image's bounding box.
    pub const fn rect(&self) -> Rect<i32> {
        Rect::new(
            Point::new(0, 0),
            Point::new(self.width as i32, self.height as i32),
        )
    }

    /// Get the pixel data as a pointer to i32 values.
    ///
    /// # Safety
    /// Safe as long as the point (x,y) is inside the image.
    pub unsafe fn pixels_mut(&mut self, x: u32, y: u32) -> *mut i32 {
        self.image_data.at_mut(self.index_of(x, y)).cast::<i32>()
    }

    /// Set a single pixel.
    pub fn put_pixel(&mut self, x: u32, y: u32, color: Rgba) {
        let index = self.index_of(x, y);

        self.image_data.set(index, color.rgba);
    }

    /// Write an image to a Writer.
    ///
    /// # Errors
    /// Forwards Writer errors.
    pub fn to_writer<W: Write>(&self, w: &mut W) -> Result<(), std::io::Error> {
        // Write BMP header
        w.write_all(&[
            0x42, 0x4D, // Header
        ])?;
        w.write_all(&(u32::from(PIXEL_DATA_OFFSET) + self.data_size_bytes()).to_le_bytes())?; // File Size
        #[rustfmt::skip]
        w.write_all(&[
            0x00, 0x00, // Reserved
            0x00, 0x00, // Reserved
            PIXEL_DATA_OFFSET, 0x00, 0x00, 0x00, // Pixel Data Offset
        ])?;

        // Write DIB Header
        w.write_all(&[DIB_HEADER_SIZE, 0x00, 0x00, 0x00])?; // Header Size
        w.write_all(&self.width.to_le_bytes())?; // Image Width
        w.write_all(&self.height.to_le_bytes())?; // Image Height

        #[rustfmt::skip]
        w.write_all(&[
            0x01, 0x00, // Planes
            32,   0x00, // Bits Per Pixel
            0x03, 0x00, 0x00, 0x00, // Compression
        ])?;
        w.write_all(&(self.width() * self.height * 4).to_le_bytes())?; // Image Size
        #[rustfmt::skip]
        w.write_all(&[
            0xe8, 0x03, 0x00, 0x00, // X Pixels Per Meter
            0xe8, 0x03, 0x00, 0x00, // Y Pixels Per Meter
            0x00, 0x00, 0x00, 0x00, // Total Colors
            0x00, 0x00, 0x00, 0x00, // Important Colors
            0x00, 0x00, 0xff, 0x00, // Red Mask
            0x00, 0xff, 0x00, 0x00, // Green Mask
            0xff, 0x00, 0x00, 0x00, // Blue Mask
            0x00, 0x00, 0x00, 0x00, // Alpha Mask
            0x42, 0x47, 0x52, 0x73, // Color Space (sRGB)
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // CIE X
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // CIE Y
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // CIE Z
            0, 0, 0, 0, // Gamma Red
            0, 0, 0, 0, // Gamma Green
            0, 0, 0, 0, // Gamma Blue
            2, 0, 0, 0, // Intent (Relative Colorimetric)
            0, 0, 0, 0, // Profile Data
            0, 0, 0, 0, // Profile Size
            0, 0, 0, 0, // Reserved
        ])?;

        for row in self
            .image_data
            .as_slice()
            .chunks_exact(self.row_width() as usize)
            .rev()
        {
            let row = unsafe {
                std::slice::from_raw_parts(row.as_ptr().cast::<u8>(), self.width as usize * 4)
            };
            w.write_all(row)?;
        }

        Ok(())
    }

    const fn data_size_bytes(&self) -> u32 {
        self.width * self.height * 4
    }

    fn row_width(&self) -> u32 {
        round_up_to_multiple_of(self.width, 8)
    }

    /// Convert (x,y) coordinates to an index into the flat image data array.
    fn index_of(&self, x: u32, y: u32) -> usize {
        let row = y;
        let col = x;
        let index = (row * self.row_width() + col) as usize;
        debug_assert!(index < self.image_data.size());

        index
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fixed_data() {
        let img = BmpImage::new(1, 1);
        let mut w = Vec::new();
        img.to_writer(&mut w).unwrap();

        assert_eq!(w[0..2], vec![0x42, 0x4D]);
        assert_eq!(w[2..6], (PIXEL_DATA_OFFSET as u32 + 4).to_le_bytes());
        assert_eq!(w[10..14], (PIXEL_DATA_OFFSET as u32).to_le_bytes());
        assert_eq!(w[14..18], (DIB_HEADER_SIZE as u32).to_le_bytes());
        assert_eq!(w[26], 1);
        assert_eq!(w[28], 32);
    }

    #[test]
    fn test_dimensions_data() {
        let img = BmpImage::new(1000, 1001);
        let mut w = Vec::new();
        img.to_writer(&mut w).unwrap();

        assert_eq!(w[18..22], (1000u32).to_le_bytes());
        assert_eq!(w[22..26], (1001u32).to_le_bytes());
    }

    #[test]
    fn test_pixel_size() {
        let img = BmpImage::new(5, 5);
        let mut w = Vec::new();
        img.to_writer(&mut w).unwrap();

        assert_eq!(w[(PIXEL_DATA_OFFSET as usize)..].len(), 25 * 4);
    }

    #[test]
    #[rustfmt::skip]
    fn test_reference_1() {
        let mut img = BmpImage::new(1, 1);
        img.put_pixel(0, 0, Rgba::new(255, 255, 255));

        let mut w = Vec::new();
        img.to_writer(&mut w).unwrap();

        assert_eq!(
            &w,
            include_bytes!("..\\..\\test\\reference_1x1_white.bmp")
        );
    }

    #[test]
    #[rustfmt::skip]
    fn test_reference_2() {
        let mut img = BmpImage::new(2, 2);
        img.put_pixel(0, 0, Rgba::new(255, 255, 255));
        img.put_pixel(1,1, Rgba::new(255, 255, 255));

        let mut w = Vec::new();
        img.to_writer(&mut w).unwrap();

        assert_eq!(
            &w,
            include_bytes!("..\\..\\test\\reference_2x2_checkerboard.bmp")
        );
    }

    #[test]
    #[rustfmt::skip]
    fn test_reference_3() {
        let mut img = BmpImage::new(5, 5);
        img.put_pixel(0, 0, Rgba::new(0, 0, 255));
        img.put_pixel(0, 4, Rgba::new(0, 255, 0));
        img.put_pixel(2, 2, Rgba::new(255, 255, 255));
        img.put_pixel(4, 0, Rgba::new(255, 0, 0));
        img.put_pixel(4, 4, Rgba::new(255, 255, 0));

        let mut w = Vec::new();
        img.to_writer(&mut w).unwrap();

        assert_eq!(
            &w,
            include_bytes!("..\\..\\test\\reference_5x5_colored.bmp")
        );
    }
}
