/// Buffer of data aligned 32 bytes.
pub struct AlignedBuffer<T, const N: usize> {
    /// Buffer data.
    mem: *mut T,
    /// Size of the buffer.
    size: usize,
}

impl<T, const N: usize> AlignedBuffer<T, N> {
    /// Create a new buffer of the given size.
    pub fn new(size: usize) -> Self {
        // Safety: Always safe.
        let mem = unsafe {
            std::alloc::alloc_zeroed(Self::layout(size * std::mem::size_of::<T>())).cast::<T>()
        };
        Self { mem, size }
    }

    /// Get the size of the buffer.
    pub const fn size(&self) -> usize {
        self.size
    }

    /// Get a pointer to the buffer, [index] bytes after the beginning.
    pub fn at(&self, index: usize) -> *const T {
        debug_assert!(index < self.size);

        // Safety: index must be less than this buffer's size.
        unsafe { self.mem.add(index) }
    }

    /// Get a pointer to the buffer, [index] bytes after the beginning.
    pub fn at_mut(&mut self, index: usize) -> *mut T {
        debug_assert!(index < self.size);

        // Safety: index must be less than this buffer's size.
        unsafe { self.mem.add(index) }
    }

    /// Memcpy a slice into the buffer.
    pub fn set(&self, index: usize, v: T) {
        debug_assert!(index < self.size);

        // Safety: index must be less than this buffer's size and v must not overlap mem.
        unsafe {
            let ptr = self.mem.add(index);
            *ptr = v;
        }
    }

    /// Get a part of the buffer as a slice.
    pub const fn as_slice(&self) -> &[T] {
        // Safety: always safe, size is always > 0.
        unsafe { std::slice::from_raw_parts(self.mem, self.size) }
    }

    /// Get a part of the buffer as a slice.
    pub fn as_slice_mut(&mut self) -> &mut [T] {
        // Safety: always safe, size is always > 0.
        unsafe { std::slice::from_raw_parts_mut(self.mem, self.size) }
    }

    /// Get the required layout to create the buffer.
    const fn layout(size: usize) -> std::alloc::Layout {
        // Safety: size > 0.
        debug_assert!(size > 0);

        unsafe { std::alloc::Layout::from_size_align_unchecked(size, N) }
    }
}

impl<T, const N: usize> Drop for AlignedBuffer<T, N> {
    fn drop(&mut self) {
        // Safety: always safe.
        unsafe {
            std::alloc::dealloc(
                self.mem.cast::<u8>(),
                Self::layout(self.size * std::mem::size_of::<T>()),
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use super::AlignedBuffer;

    #[test]
    fn new_drop() {
        let _ = AlignedBuffer::<u8, 32>::new(1);
    }

    #[test]
    fn size() {
        let buf = AlignedBuffer::<u32, 32>::new(3);
        assert_eq!(buf.size(), 3);
    }

    #[test]
    fn at() {
        let buf = AlignedBuffer::<u8, 32>::new(5);
        let off = buf.at(4);

        unsafe {
            assert_eq!(off, buf.mem.offset(4));
        }
    }

    #[test]
    fn at_mut() {
        let mut buf = AlignedBuffer::<f32, 32>::new(7);
        let off = buf.at_mut(4);

        assert_eq!(off, unsafe { buf.mem.offset(4) });
    }

    #[test]
    fn set() {
        let buf = AlignedBuffer::<i32, 32>::new(9);
        let expected = 0x12345678;

        buf.set(4, expected);

        assert_eq!(unsafe { *buf.at(4) }, expected);
    }

    #[test]
    fn as_slice() {
        let buf = AlignedBuffer::<u32, 32>::new(10);
        let expected = 0x12345678;

        buf.set(7, expected);

        assert_eq!(buf.as_slice()[7..], [expected, 0, 0]);
    }
}
