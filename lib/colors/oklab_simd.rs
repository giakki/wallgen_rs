use crate::math::float_approx_eq;

use std::arch::x86_64::*;

use super::rgba::Rgba;

#[derive(Debug, Clone, Copy)]
pub struct OkLabSimd {
    l: f64,
    a: f64,
    b: f64,
}

impl PartialEq for OkLabSimd {
    fn eq(&self, other: &Self) -> bool {
        float_approx_eq(self.l, other.l)
            && float_approx_eq(self.a, other.a)
            && float_approx_eq(self.b, other.b)
    }
}

impl OkLabSimd {
    pub const fn new(l: f64, a: f64, b: f64) -> Self {
        Self { l, a, b }
    }

    pub fn interpolate(&self, other: &Self, ratio: f64) -> Self {
        unsafe {
            // for c in (l, a, b) -> c = self.c + ratio * (other.c - self.c)
            //                    -> c = (self.c + ratio * other.c) - (ratio * self.c)

            let self_lab = _mm256_set_pd(self.l, self.a, self.b, 0.);
            let other_lab = _mm256_set_pd(other.l, other.a, other.b, 0.);
            let ratio = _mm256_set1_pd(ratio);

            let intermediate = _mm256_sub_pd(other_lab, self_lab);
            let result = _mm256_fmadd_pd(ratio, intermediate, self_lab);
            let [_, b, a, l] = Self::_mm256_as_slice(result);

            Self::new(l, a, b)
        }
    }

    pub fn interpolate2(&self, other: &Self, ratio: f64) -> Self {
        unsafe {
            // for c in (l, a, b) -> c = self.c + ratio * (other.c - self.c)
            //                    -> c = (self.c + ratio * other.c) - (ratio * self.c)
            let self_lab = _mm256_set_pd(self.l, self.a, self.b, 0.);
            let other_lab = _mm256_set_pd(other.l, other.a, other.b, 0.);
            let ratio = _mm256_set1_pd(ratio);

            let l0 = _mm256_fmadd_pd(ratio, other_lab, self_lab);
            let l1 = _mm256_mul_pd(ratio, self_lab);
            let result = _mm256_sub_pd(l0, l1);

            let [_, b, a, l] = Self::_mm256_as_slice(result);

            Self::new(l, a, b)
        }
    }

    pub fn as_rgba(&self) -> Rgba {
        unsafe {
            let lms = Self::vec_by_matrix(
                [self.l, self.a, self.b, 0.],
                [
                    _mm256_set1_pd(1.),
                    _mm256_set_pd(0.3963377774, -0.1055613458, -0.0894841775, 0.),
                    _mm256_set_pd(0.2158037573, -0.0638541728, -1.291485548, 0.),
                    _mm256_set1_pd(0.),
                ],
            );

            let lms3 = _mm256_mul_pd(lms, _mm256_mul_pd(lms, lms));
            let [_, s, m, l] = Self::_mm256_as_slice(lms3);

            let lrgb = Self::vec_by_matrix(
                [l, m, s, 0.],
                [
                    _mm256_set_pd(4.0767416621, -1.2684380046, -0.0041960863, 0.),
                    _mm256_set_pd(-3.3077115913, 2.6097574011, -0.7034186147, 0.),
                    _mm256_set_pd(0.2309699292, -0.3413193965, 1.707614701, 0.),
                    _mm256_set1_pd(0.),
                ],
            );

            let [_, lb, lg, lr] = Self::_mm256_as_slice(lrgb);
            let rgb = Self::lrgb2rgb_simd(lr, lg, lb);

            let [_, b, g, r] = Self::_mm256_as_slice(_mm256_round_pd::<0>(_mm256_mul_pd(
                rgb,
                _mm256_set1_pd(255.),
            )));

            Rgba::new(r as u8, g as u8, b as u8)
        }
    }

    unsafe fn lrgb2rgb_simd(r: f64, g: f64, b: f64) -> __m256d {
        let rgb = _mm256_set_pd(r, g, b, 0.);
        let gamma = _mm256_set1_pd(0.0031308);
        let mask_gte_gamma = _mm256_cmp_pd::<0x1a>(rgb, gamma);
        // If branch
        let if_b = _mm256_set_pd(r.powf(1.0 / 2.4), g.powf(1.0 / 2.4), b.powf(1.0 / 2.4), 0.);
        let if_b = _mm256_fmsub_pd(if_b, _mm256_set1_pd(1.055), _mm256_set1_pd(0.055));
        // Else branch
        let else_b = _mm256_mul_pd(rgb, _mm256_set1_pd(12.92));

        _mm256_blendv_pd(if_b, else_b, mask_gte_gamma)
    }

    unsafe fn _mm256_as_slice(reg: __m256d) -> [f64; 4] {
        let mut res = [0.; 4];
        _mm256_storeu_pd(res.as_mut_ptr(), reg);
        res
    }

    unsafe fn vec_by_matrix(
        vec: [f64; 4],
        mat: [std::arch::x86_64::__m256d; 4],
    ) -> std::arch::x86_64::__m256d {
        let x = _mm256_set1_pd(vec[0]);
        let y = _mm256_set1_pd(vec[1]);
        let z = _mm256_set1_pd(vec[2]);
        let w = _mm256_set1_pd(vec[3]);

        let p1 = _mm256_mul_pd(x, mat[0]);
        let p2 = _mm256_fmadd_pd(y, mat[1], p1);
        let p3 = _mm256_mul_pd(z, mat[2]);
        let p4 = _mm256_fmadd_pd(w, mat[3], p3);

        _mm256_add_pd(p2, p4)
    }
}

#[cfg(test)]
mod tests {
    use crate::colors::oklab::OkLab;

    use super::*;

    #[test]
    fn simd_and_non_simd_interpolate_should_be_equal() {
        let a = OkLabSimd::new(0.9932119474, -0.0098932686, 0.0323591562);
        let b = OkLabSimd::new(0.3440683982, -0.0741425435, 0.0290086733);
        let c = OkLab::new(0.9932119474, -0.0098932686, 0.0323591562);
        let d = OkLab::new(0.3440683982, -0.0741425435, 0.0290086733);

        let actual = a.interpolate(&b, 0.5);
        let expected = c.interpolate(&d, 0.5);

        assert_eq!(actual.l, expected.l);
        assert_eq!(actual.a, expected.a);
        assert_eq!(actual.b, expected.b);
    }

    #[test]
    fn simd2_and_non_simd_interpolate_should_be_equal() {
        let a = OkLabSimd::new(0.9932119474, -0.0098932686, 0.0323591562);
        let b = OkLabSimd::new(0.3440683982, -0.0741425435, 0.0290086733);
        let c = OkLab::new(0.9932119474, -0.0098932686, 0.0323591562);
        let d = OkLab::new(0.3440683982, -0.0741425435, 0.0290086733);

        let actual = a.interpolate2(&b, 0.5);
        let expected = c.interpolate(&d, 0.5);

        assert_eq!(actual.l, expected.l);
        assert_eq!(actual.a, expected.a);
        assert_eq!(actual.b, expected.b);
    }

    #[test]
    fn simd_and_non_simd_to_rgb_should_be_equal() {
        let a = OkLabSimd::new(0.9932119474, -0.0098932686, 0.0323591562);
        let b = OkLabSimd::new(0.3440683982, -0.0741425435, 0.0290086733);
        let c = OkLab::new(0.9932119474, -0.0098932686, 0.0323591562);
        let d = OkLab::new(0.3440683982, -0.0741425435, 0.0290086733);

        assert_eq!(a.as_rgba(), c.as_rgba());
        assert_eq!(b.as_rgba(), d.as_rgba());
    }
}
