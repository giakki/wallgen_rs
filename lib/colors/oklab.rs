use super::rgba::Rgba;
use crate::math::float_approx_eq;

/// Oklab color space.
/// [ref.](https://bottosson.github.io/posts/oklab/)
#[derive(Debug, Clone, Copy)]
pub struct OkLab {
    /// L
    pub l: f64,
    /// A
    pub a: f64,
    /// B
    pub b: f64,
}

impl PartialEq for OkLab {
    /// Compare two ``OkLab`` colors by value.
    fn eq(&self, other: &Self) -> bool {
        float_approx_eq(self.l, other.l)
            && float_approx_eq(self.a, other.a)
            && float_approx_eq(self.b, other.b)
    }
}

impl OkLab {
    /// Construct a new color.
    pub const fn new(l: f64, a: f64, b: f64) -> Self {
        Self { l, a, b }
    }

    /// Get a color [ratio]% of the way between two colors
    pub fn interpolate(&self, other: &Self, ratio: f64) -> Self {
        Self::new(
            ratio.mul_add(other.l - self.l, self.l),
            ratio.mul_add(other.a - self.a, self.a),
            ratio.mul_add(other.b - self.b, self.b),
        )
    }

    /// Transform from ``OkLab`` to ``sRgb`` color space.
    /// Implementation from the official docs.
    /// [ref.](https://bottosson.github.io/posts/oklab/#converting-from-linear-srgb-to-oklab)
    pub fn as_rgba(&self) -> Rgba {
        // Translation example:
        // self.l + self.a * 0.3963377774 + self.b * 0.2158037573
        // a * 0... + b * 0... + l
        let l = self
            .a
            .mul_add(0.3963377774, self.b.mul_add(0.2158037573, self.l))
            .powi(3);
        let m = self
            .a
            .mul_add(-0.1055613458, self.b.mul_add(-0.0638541728, self.l))
            .powi(3);
        let s = self
            .a
            .mul_add(-0.0894841775, self.b.mul_add(-1.291485548, self.l))
            .powi(3);

        Rgba::new(
            (lrgb2rgb(l.mul_add(4.0767416621, m.mul_add(-3.3077115913, s * 0.2309699292)))).round()
                as u8,
            (lrgb2rgb(l.mul_add(-1.2684380046, m.mul_add(2.6097574011, s * -0.3413193965)))).round()
                as u8,
            (lrgb2rgb(l.mul_add(-0.0041960863, m.mul_add(-0.7034186147, s * 1.707614701)))).round()
                as u8,
        )
    }
}

/// Gamma correction conversion linear RGB (0..1) to ``sRGB`` (0..255).
fn lrgb2rgb(f: f64) -> f64 {
    let srgb = if f >= 0.0031308_f64 {
        1.055_f64 * f.powf(1.0 / 2.4) - 0.055_f64
    } else {
        12.92_f64 * f
    };

    255. * srgb
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn as_rgba() {
        // Data taken fom chroma.js.
        // https://github.com/gka/chroma.js/blob/main/test/oklab2rgb.test.js
        let tests = [
            ("black", [0.0, 0.0, 0.0], [0, 0, 0, 1]),
            ("white", [1.0, 0.0, 0.0], [255, 255, 255, 1]),
            ("gray", [0.59987, 0.0, 0.0], [128, 128, 128, 1]),
            ("red", [0.62796, 0.22486, 0.12585], [255, 0, 0, 1]),
            ("yellow", [0.96798, -0.07137, 0.19857], [255, 255, 0, 1]),
            ("green", [0.51975, -0.1403, 0.10768], [0, 128, 0, 1]),
            ("cyan", [0.9054, -0.14944, -0.0394], [0, 255, 255, 1]),
            ("blue", [0.45201, -0.03246, -0.31153], [0, 0, 255, 1]),
            ("magenta", [0.70167, 0.27457, -0.16916], [255, 0, 255, 1]),
        ];

        for (name, lab, rgb) in tests {
            let oklab = OkLab::new(lab[0], lab[1], lab[2]);
            let rgba = Rgba::new(rgb[0], rgb[1], rgb[2]);

            assert_eq!(
                oklab.as_rgba(),
                rgba,
                "Color '{}' different from expected",
                name
            );
        }
    }

    #[test]
    fn interpolate() {
        #[rustfmt::skip]
        let tests = [
            ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], 0., [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]),
            ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], 0.5, [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]),
            ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], 1., [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]),
            ([0.0, 0.0, 0.0], [1.0, 1.0, 1.0], 0., [0.0, 0.0, 0.0], [1.0, 1.0, 1.0]),
            ([0.0, 0.0, 0.0], [1.0, 1.0, 1.0], 0.5, [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
            ([0.0, 0.0, 0.0], [1.0, 1.0, 1.0], 1., [1.0, 1.0, 1.0], [0.0, 0.0, 0.0]),
            ([0.333, 0.666, 0.999], [0.999, 0.666, 0.333], 0.5, [0.666, 0.666, 0.666], [0.666, 0.666, 0.666]),
        ];

        for (start, end, ratio, expected, inv_expected) in tests {
            let start = OkLab::new(start[0], start[1], start[2]);
            let end = OkLab::new(end[0], end[1], end[2]);
            let expected = OkLab::new(expected[0], expected[1], expected[2]);
            let inv_expected = OkLab::new(inv_expected[0], inv_expected[1], inv_expected[2]);

            assert_eq!(start.interpolate(&end, ratio), expected);
            assert_eq!(end.interpolate(&start, ratio), inv_expected);
        }
    }
}
