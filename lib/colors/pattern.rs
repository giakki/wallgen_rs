use crate::geometry::{point::Point, shapes::Triangle};

use super::{oklab::OkLab, rgba::Rgba};

/// Generic pattern trait.
/// Used to create gradients.
pub trait Pattern {
    /// Apply the pattern to a triangle.
    fn apply(&self, triangle: &Triangle) -> Rgba;
}

/// Simple radial gradient.
pub struct RadialGradient<'color> {
    /// The distance from the start point to the end point.
    range: f64,
    /// Start point.
    origin: Point<i32>,
    /// Sequence of stops with a distance from the origin and an associated color.
    stops: Vec<(f64, &'color OkLab)>,
}

impl<'color> RadialGradient<'color> {
    /// Create a new radial gradient emanating from <origin> and ending at <end>.
    pub fn new(origin: Point<i32>, end: Point<i32>, colors: &'color [OkLab]) -> Self {
        let range = f64::from((origin.x + end.x).pow(2) + (origin.y + end.y).pow(2)).sqrt();
        // Assign each color to a distance from the origin.
        let stops = colors
            .iter()
            .enumerate()
            .map(|(i, color)| (i as f64 / (colors.len() - 1) as f64, color))
            .collect::<Vec<_>>();

        Self {
            range,
            origin,
            stops,
        }
    }
}

impl<'color> Pattern for RadialGradient<'color> {
    fn apply(&self, triangle: &Triangle) -> Rgba {
        let distance_from_origin = triangle.distance_from(&self.origin);
        let ratio_from_origin = (distance_from_origin / self.range).clamp(0., 1.);

        // Find the two stops that contain the point
        let (start, end) = self
            .stops
            .windows(2)
            .find(|w| ratio_from_origin >= w[0].0 && ratio_from_origin <= w[1].0)
            .map_or_else(|| (self.stops[0], self.stops[1]), |cols| (cols[0], cols[1]));

        let (start_ratio, start_color) = start;
        let (end_ratio, end_color) = end;

        let ratio_between_steps = (ratio_from_origin - start_ratio) / (end_ratio - start_ratio);

        start_color
            .interpolate(end_color, ratio_between_steps)
            .as_rgba()
    }
}
