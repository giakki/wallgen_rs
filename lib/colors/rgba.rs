/// ``sRGB`` color space.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Rgba {
    /// Color representation as BRGA.
    pub rgba: u32,
}

impl Rgba {
    /// Create an Rgba colors with opacity 1.
    pub const fn new(r: u8, g: u8, b: u8) -> Self {
        Self {
            rgba: u32::from_be_bytes([0, g, r, b]),
        }
    }
}
