use criterion::{criterion_group, criterion_main, Criterion};
use lib::{
    colors::rgba::Rgba,
    geometry::{point::Point, shapes::Triangle},
    image::bmp_image::BmpImage,
};

fn run_barycentric(canvas: &mut BmpImage, triangle: &Triangle, color: Rgba) {
    lib::rasterizer::barycentric::rasterize_triangle(canvas, triangle, color);
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("Rasterizer");

    let mut image = BmpImage::new(32, 32);
    let triangle = Triangle::new(
        Point::new(0., 0.),
        Point::new(0., 52.),
        Point::new(64., 52.),
    );
    let color: Rgba = Rgba::new(0x12, 0x34, 0x56);

    group.bench_function("barycentric", |b| {
        b.iter(|| run_barycentric(&mut image, &triangle, color))
    });

    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
