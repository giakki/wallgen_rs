use criterion::{black_box, criterion_group, criterion_main, Criterion};
use lib::colors::{oklab::OkLab, oklab_simd::OkLabSimd, rgba::Rgba};

fn run_simple((a, b): &(OkLab, OkLab)) -> Rgba {
    a.interpolate(b, 0.5).as_rgba()
}

fn run_simd((a, b): &(OkLabSimd, OkLabSimd)) -> Rgba {
    a.interpolate(b, 0.5).as_rgba()
}

fn run_simd2((a, b): &(OkLabSimd, OkLabSimd)) -> Rgba {
    a.interpolate2(b, 0.5).as_rgba()
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("Color generation");

    let a = OkLab::new(0.992, -0.03, 0.095);
    let b = OkLab::new(0.271, -0.0475, -0.4675);

    let c = OkLabSimd::new(0.992, -0.03, 0.095);
    let d = OkLabSimd::new(0.271, -0.0475, -0.4675);

    group.bench_with_input("Simple", &(a, b), |b, &args| {
        b.iter(|| black_box(run_simple(&args)))
    });

    group.bench_with_input("SIMD", &(c, d), |b, &args| {
        b.iter(|| black_box(run_simd(&args)))
    });

    group.bench_with_input("SIMD2", &(c, d), |b, &args| {
        b.iter(|| black_box(run_simd2(&args)))
    });

    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
