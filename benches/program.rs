use criterion::{black_box, criterion_group, criterion_main, Criterion};
use lib::{colors::oklab::OkLab, image::bmp_image::BmpImage, options::Options, run};
use rand::{rngs::StdRng, SeedableRng};

const A: OkLab = OkLab::new(0.992, -0.03, 0.095);
const B: OkLab = OkLab::new(0.271, -0.0475, -0.4675);

fn bench_run(options: Options) -> BmpImage {
    run(options)
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("Full program run");

    let options = Options {
        colors: &[A, B],
        height: 1440,
        rng: StdRng::from_seed([0; 32]),
        scale: 200.,
        width: 3440,
    };

    group.bench_with_input("run", &options, |b, args| {
        b.iter(|| black_box(bench_run(args.clone())))
    });

    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
